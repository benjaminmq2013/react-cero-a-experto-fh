// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth"
import { getFirestore } from "firebase/firestore/lite"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyC_6USBio6UBGpbhSxr_gjfNMBFU1x6E1U",
  authDomain: "open-jira-react.firebaseapp.com",
  projectId: "open-jira-react",
  storageBucket: "open-jira-react.appspot.com",
  messagingSenderId: "743833410154",
  appId: "1:743833410154:web:9fd104abdf308c85784813"
};

// Initialize Firebase
export const FirebaseApp = initializeApp(firebaseConfig);
export const FirebaseAuth = getAuth(FirebaseApp)
export const FirebaseDB = getFirestore(FirebaseApp)