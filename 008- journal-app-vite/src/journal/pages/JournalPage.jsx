import { AddOutlined } from "@mui/icons-material"
import { IconButton } from "@mui/material"
import { NoteView, NothingSelectedView } from "../../views"
import { JournalLayout } from "../layout/JournalLayout"

export const JournalPage = () => {
  return (
    <JournalLayout>
      {/* <Typography>Labore duis deserunt eiusmod ipsum do non sit eiusmod nostrud eu incididunt aute voluptate dolor.</Typography> */}
      <NothingSelectedView />

      {/* <NoteView /> */}


      <IconButton 
        size="large"
        sx={{
          color: "white",
          backgroundColor: "error.main",
          ":hover": { backgroundColor: "error.main", opacity: 0.9 },
          position: "fixed",
          right: 50,
          bottom: 50,
        }}
      >
        <AddOutlined sx={{ fontSize: 30 }} />
      </IconButton>
    </JournalLayout>

    )
}
