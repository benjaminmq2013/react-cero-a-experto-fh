import { MenuOutlined, LogoutOutlined } from "@mui/icons-material"
import { AppBar, Grid, IconButton, Toolbar, Typography } from "@mui/material"
import { Link as RouterLink }  from "react-router-dom"

export const NavBar = ({ drawerWidth = 280 }) => {
  return (
    <AppBar 
      position="fixed"
      sx={{ 
        width: { sm: `calc(100% - ${ drawerWidth }px)` },
        ml: { sm: `${ drawerWidth }` }
      }}
    >
      <Toolbar>
        <IconButton
          color= "inherit"
          edge="start"
          sx={{ mr: 2, display: { sm: "none" } }}
        >
          <MenuOutlined />
        </IconButton>

        <Grid container direction="row" justifyContent="space-around" alignItems="center" >
          <Typography variant="h6" noWrap component="div"> JournalApp </Typography>

          <IconButton>
          <RouterLink to="/auth/login">
            <LogoutOutlined color="error"/>
          </RouterLink>
          </IconButton>
        </Grid>
      </Toolbar>
    </AppBar>  
    
  )
}
