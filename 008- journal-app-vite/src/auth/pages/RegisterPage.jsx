import { Link as RouterLink }  from "react-router-dom"
import { Grid, Typography, TextField, Button, Link } from "@mui/material"
import { AuthLayout } from "../layout/AuthLayout";
import { useForm } from "../../hooks";
import { useState } from "react";
// import TextField from "@mui/material/TextField";
const formData = {
  email: "benjaminmq2013@gmail.com",
  password: "123456",
  displayName: "Adriel Benjamin"
}

const formValidations = {
  email: [(value) => value.includes("@"), 'El correo debe de tener una @'],
  password: [(value) => value.length >= 6, "El password debe tener más de 6 caracteres" ],
  displayName: [(value) => value.length >= 1, "El nombre es obligatorio" ],
}

export const RegisterPage = () => {

  const [formSubmitted, setFormSubmitted] = useState(false)

  const { 
    formState, displayName, email, password, onInputChange, 
    isFormValid, displayNameValid, emailValid, passwordValid,
  } = useForm(formData, formValidations)

  
  const onSubmit = ( event ) => {
    event.preventDefault()

    if( !isFormValid ) return;

    setFormSubmitted(true)
  }

  return (
    <AuthLayout title="Crear cuenta">
      <h1>FormValid{ isFormValid ? "Valido": "Incorrecto" }</h1>
      <form onSubmit={ onSubmit }>
      <Grid container>
        <Grid item xs={12} sx={{ mt: 2 }}>
          <TextField
            label="Nombre completo"
            type="text"
            placeholder="correo@google.com"
            fullWidth
            name="displayName"
            value={ displayName }
            onChange={ onInputChange }
            error={ !!displayNameValid && formSubmitted }
            helper={ displayNameValid }
          />
        </Grid>

        <Grid item xs={12} sx={{ mt: 2 }}>
          <TextField
            label="email"
            type="email"
            placeholder="correo@google.com"
            fullWidth
            name="email"
            value={ email }
            onChange={ onInputChange }
            error={ !!emailValid && formSubmitted }
            helper={ emailValid }
          />
        </Grid>

        <Grid item xs={12} sx={{ mt: 2 }}>
          <TextField
            label="password"
            type="password"
            placeholder="password"
            fullWidth
            name="password"
            value={ password }
            onChange={ onInputChange }
            error={ !!passwordValid && formSubmitted }
            helper={ passwordValid }
          />
        </Grid>

        <Grid container spacing={2} sx={{ mb: 2, mt: 1 }}>
          <Grid item xs={12} sm={6}>
            <Button 
              type="submit"
              variant="contained" 
              fullWidth
            >
              Crear cuenta
            </Button>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Button variant="contained" fullWidth>
              <Typography sx={{ ml: 1 }}>Google</Typography>
            </Button>
          </Grid>
        </Grid>

        <Grid container direction="row" justifyContent="end">
          <Link component={RouterLink} color="inherit" to="/auth/login">
            ¿Ya tienes una cuenta?
          </Link>
        </Grid>
      </Grid>
    </form>
    </AuthLayout>
    
  );
}
