import { Link as RouterLink }  from "react-router-dom"
import { Google } from "@mui/icons-material";
import { Grid, Typography, TextField, Button, Link } from "@mui/material"
import { AuthLayout } from "../layout/AuthLayout";
import { useForm } from "../../hooks";
import { useDispatch, useSelector } from "react-redux";
import { checkingAuthentication, startGoogleSignIn } from "../../store/auth/thunks";
import { useMemo } from "react";
// import TextField from "@mui/material/TextField";

export const LoginPage = () => {

  const { status } = useSelector( state => state.auth )

  const dispatch = useDispatch()

  const { email, password, onInputChange } = useForm({
    email: "benjaminmq@google.com",
    password: "123456"
  })

  const isAuthenticating = useMemo(()=> status === "checking", [status] )

  const onSubmit = (event) => {
    event.preventDefault();
    console.log({ email, password })
    dispatch(checkingAuthentication())
  }
  
  const onGoogleSignIn = () => {
    console.log("onGoogleSignIn")
    dispatch( startGoogleSignIn() )
  }

  return (
    <AuthLayout title="login">
      <form onSubmit={ onSubmit }>
      <Grid container>
        <Grid item xs={12} sx={{ mt: 2 }}>
          <TextField
            label="correo"
            type="email"
            placeholder="correo@google.com"
            fullWidth
            name="email"
            value={ email }
            onChange={ onInputChange }
          />
        </Grid>

        <Grid item xs={12} sx={{ mt: 2 }}>
          <TextField
            label="password"
            type="password"
            placeholder="password"
            fullWidth
            name="password"
            value={ password }
            onChange={ onInputChange }
          />
        </Grid>

        <Grid container spacing={2} sx={{ mb: 2, mt: 1 }}>
          <Grid item xs={12} sm={6}>
            <Button 
              disabled= { isAuthenticating }
              type="submit" 
              variant="contained" 
              fullWidth
              isAuthenticating
            >
              Login
            </Button>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Button
              disabled= { isAuthenticating }
              variant="contained"
              fullWidth
              onClick={ onGoogleSignIn }
            >
              <Google />
              <Typography sx={{ ml: 1 }}>Google</Typography>
            </Button>
          </Grid>
        </Grid>

        <Grid container direction="row" justifyContent="end">
          <Link component={RouterLink} color="inherit" to="/auth/register">
            Crear una cuenta
          </Link>
        </Grid>
      </Grid>
    </form>
    </AuthLayout>
    
  );
}
