import { useEffect, useState } from "react"
import { getPokemons, pokemonSlice } from "./store/slices/pokemons";
import { useDispatch, useSelector } from "react-redux/es/exports";

export const PokemonApp = () => {
  // const [page, setPage] = useState(1)


  const dispatch = useDispatch();
  const { pokemons, isLoading, page } = useSelector((state)=> state.pokemons)
  console.log(pokemons)

  useEffect(() => {
    dispatch(getPokemons())
  }, [])

  
  return (
    <>
        <h1>PokemonApp</h1>
        <hr />
        <span>Loading: { isLoading? "True":"false" }</span>

        <ol>  
            { pokemons.map(pokemon => (
              <li key={ pokemon.name }> { pokemon.name } </li>
            )) }
        </ol>

        <button
          disabled = { isLoading }
          onClick={ ()=> dispatch(getPokemons(page + 1)) }
        >
          Ver más
        </button>
    </>
  )
}
