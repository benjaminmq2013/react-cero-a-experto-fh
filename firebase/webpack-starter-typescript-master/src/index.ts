import db from "./firebase/config"
import { collection, addDoc, getDocs } from "firebase/firestore";

const usuario = {
    nombre: "Maria",
    active: false,
    fechaNaci: 0
}

const postCollection = async () =>{
    try{
        const docRef = await addDoc(collection(db, "users"), usuario)
        console.log("Document written with ID: ", docRef)
    } catch (e) {
        console.error("Error adding document: ", e)
    }                                                
}
// postCollection()


