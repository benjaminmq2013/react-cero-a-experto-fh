import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore"
import "firebase/firestore"

const firebaseConfig = {
    apiKey: "AIzaSyDSOJGTDspL_9_4Qc7zAzbmGaKHGdvgU5M",
    authDomain: "sql-demos-fa57d.firebaseapp.com",
    projectId: "sql-demos-fa57d",
    storageBucket: "sql-demos-fa57d.appspot.com",
    messagingSenderId: "952640165670",
    appId: "1:952640165670:web:e71f4d0ae4780494ab53e7",
    measurementId: "G-Q73SKK88R4"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app)

export default db